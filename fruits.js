db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// [Section] MongoDB Aggregation

/*
MongoDB Aggregation
-Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
-Compared to doing CRUD operations, aggregation gives us access to manipulate, filter, and compute for results providing us with information to make necessary development decisions without having to create a frontend application.


*/

// Using the Aggregate Method

/*
  $match is used to pass the documents that meet the specified condition(s) the next pipeline stage/aggregation process
    -Syntax:
        {$match: {field:value}}
*/

db.fruits.aggregate([
  {$match: {onSale:true}}
]);

// $group is used to group elements together and field-value pairs, using the data from the grouped elements

/*
  -Syntax:
    ($group: {_id:"value", fieldResult:"valueResult"});
*/

db.fruits.aggregate([

  {$group:{_id:"$supplier_id", 
  total:{$sum: "$stock"}}}
  ]);


// Aggregating Documents using 2 pipeline stages

/*
  Syntax:
    db.collectionName.aggregate([
      {$match: {field:value}},
      {$group: {_id:"value", fieldResult:"valueResult"}};
    ])

*/


db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"$supplier_id", 
  total: {$sum: "$stock"}}}
]);

// Field Projection with aggregation

/*
  $project can be used when aggregating data to include/exclude fields from the returned results
  -The field that is set to zero(0) will be excluded
  -The field that is set to one(1) will be included


  Syntax:
    {$project:{field:1/0}}
*/

db.fruits.aggregate([
  {$project:{_id:0}}

  ]);


db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"$supplier_id", 
  total: {$sum: "$stock"}}},
  {$project:{_id:0}}
]);


// Sorting aggregated results
/*
  $sort can be used to changed the order the aggregated results
  -Providing a value of -1 will sort the aggregated results in a REVERSE ORDER

  Syntax:
    {$sort{field: 1/-1}}

*/

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"$supplier_id", 
  total: {$sum: "$stock"}}},
  {$sort:{total:-1}}
]);

// The field that is set to a value of negative (-1) was sorted in descending order
// The field that is set to a value of positive (1) was sorted in ascending order

// Aggregating results based on the array fields
/*
  "$unwind" deconstructs an array fields from a collection with an array value to give us a result for each array elements

  Syntax:
    {$unwind:field}
*/

db.fruits.aggregate([
    {$unwind: "$origin"}
  ]);

// Display fruits documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: "$origin", kinds: {$sum:1}}}
  ]);